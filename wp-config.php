<?php
/**
 * Cấu hình cơ bản cho WordPress
 *
 * Trong quá trình cài đặt, file "wp-config.php" sẽ được tạo dựa trên nội dung 
 * mẫu của file này. Bạn không bắt buộc phải sử dụng giao diện web để cài đặt, 
 * chỉ cần lưu file này lại với tên "wp-config.php" và điền các thông tin cần thiết.
 *
 * File này chứa các thiết lập sau:
 *
 * * Thiết lập MySQL
 * * Các khóa bí mật
 * * Tiền tố cho các bảng database
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Thiết lập MySQL - Bạn có thể lấy các thông tin này từ host/server ** //
/** Tên database MySQL */
define( 'DB_NAME', 'wpbase' );

/** Username của database */
define( 'DB_USER', 'root' );

/** Mật khẩu của database */
define( 'DB_PASSWORD', '' );

/** Hostname của database */
define( 'DB_HOST', 'localhost' );

/** Database charset sử dụng để tạo bảng database. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Kiểu database collate. Đừng thay đổi nếu không hiểu rõ. */
define('DB_COLLATE', '');

/**#@+
 * Khóa xác thực và salt.
 *
 * Thay đổi các giá trị dưới đây thành các khóa không trùng nhau!
 * Bạn có thể tạo ra các khóa này bằng công cụ
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Bạn có thể thay đổi chúng bất cứ lúc nào để vô hiệu hóa tất cả
 * các cookie hiện có. Điều này sẽ buộc tất cả người dùng phải đăng nhập lại.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'djHuBtL|i2Sul^#Q0wJ+{aLdo,PMg(!dO/lJ74`^0k{!AhV-N.kOGDu<G/5**9}e' );
define( 'SECURE_AUTH_KEY',  '^Ri[e7uXS5Gs&]t?!GnaPLfLiwZ5nz_ofe[f2<}x^!s#7;)BK@q5z)a&u>:/,ef}' );
define( 'LOGGED_IN_KEY',    '72nH,F!}#w^0KE>(p0oYuC^rJ.D){z<@<BJw5ccXI` _]td$J3&V@ToUXq VqkkS' );
define( 'NONCE_KEY',        '=o^ME EZ4wCDQ7;Z?^iu[CTWg#su`q_r=;{Y4j+VS*&l.=0j^91943Jy?5y}#/iW' );
define( 'AUTH_SALT',        '[R4#(h:i}4%frEtZD@=j>=KAT:o$DA)`YkRsc,JbwX-X3i&#j|RdML]^NE-;p|LS' );
define( 'SECURE_AUTH_SALT', '~dfKswHF-<^0tr4A271pE dR-(M~xhvwvcls2Np ~uG,QkI{kRz#&cN`O&fvgZ@o' );
define( 'LOGGED_IN_SALT',   ']:kWsxKU<vx744O<an7.>~ZW]-s%`6/rGmY37ESZCa+2]O[vrLYu`>~@`#Al}UMw' );
define( 'NONCE_SALT',       '#bK|4V_-Y%G:7B<Gd.,1Pd<`Q`l!Sg?Jt)g6C05lW$}YXrE[Q.k_uw+b|Le~{&km' );

/**#@-*/

/**
 * Tiền tố cho bảng database.
 *
 * Đặt tiền tố cho bảng giúp bạn có thể cài nhiều site WordPress vào cùng một database.
 * Chỉ sử dụng số, ký tự và dấu gạch dưới!
 */
$table_prefix = 'wp_';

/**
 * Dành cho developer: Chế độ debug.
 *
 * Thay đổi hằng số này thành true sẽ làm hiện lên các thông báo trong quá trình phát triển.
 * Chúng tôi khuyến cáo các developer sử dụng WP_DEBUG trong quá trình phát triển plugin và theme.
 *
 * Để có thông tin về các hằng số khác có thể sử dụng khi debug, hãy xem tại Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Đó là tất cả thiết lập, ngưng sửa từ phần này trở xuống. Chúc bạn viết blog vui vẻ. */

/** Đường dẫn tuyệt đối đến thư mục cài đặt WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Thiết lập biến và include file. */
require_once(ABSPATH . 'wp-settings.php');
